﻿using EmailApplication;
using System;

namespace ProgrammingGuide
{
    // Another class definition that contains Main, the program entry point.
    class Program
    {
        static void Main(string[] args)
        {
            // Create an object of type Donor.
            Donor donor = new Donor("mohit", "mohituprim@gmail.com", "8527331928");
            IUser user = donor.SaveUser();
            string from = "mohituprim@gmail.com";
            string displayName = "Mohit";
            if (user != null)
            {
                EmailMessage message = new EmailMessage();
                message.Subject = "Using the new SMTP client.";
                message.Body = @"Using this new feature, you can send an email message from an application very easily.";
                
                EmailService.SendEmail(user, from, displayName, message);
            }
        }
    }
}
// The example displays the following output:
//      The result is 108.