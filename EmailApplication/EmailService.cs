﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;

namespace EmailApplication
{
    public static class EmailService
    {
        static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }
        public static void SendEmail(IUser user, string emailFrom, string displayName, EmailMessage emailMessage)
        {
            // Command-line argument must be the SMTP host.
            string smtpHost = "abc";
            SmtpClient client = new SmtpClient(smtpHost);
            // Specify the email sender.
            MailAddress from = new MailAddress(emailFrom, displayName, System.Text.Encoding.UTF8);
            // Set destinations for the email message.
            MailAddress to = new MailAddress(user.Email);
            // Specify the message content.
            MailMessage message = new MailMessage(from, to);
            message.Body = emailMessage.Body;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = emailMessage.Subject;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            // Set the method that is called back when the send operation ends.
            client.SendCompleted += new
            SendCompletedEventHandler(SendCompletedCallback);
            // The userState can be any object that allows your callback 
            // method to identify this send operation.
            // For this example, the userToken is a string constant.
            string userState = "test message1";

            // Enable this line with valid smtp host
            //client.SendAsync(message, userState);
            Console.WriteLine("Sending message... press c to cancel mail. Press any other key to exit.");
            string answer = Console.ReadLine();
            // If the user canceled the send, and mail hasn't been sent yet,
            // then cancel the pending operation.
            if (answer.StartsWith("c") && mailSent == false)
            {
                client.SendAsyncCancel();
            }
            // Clean up.
            message.Dispose();
            Logger.WriteLine("email sent.");
        }
    }
}
