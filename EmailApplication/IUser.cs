﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplication
{
    public interface IUser
    {
         String Name { get; set; }
         String Email { get; set; }
         String PhoneNo { get; set; }
         IUser SaveUser();
    }
}
