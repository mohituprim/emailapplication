﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplication
{
    public class EmailMessage
    {
        // Class members.
        //
        // Property.
        public String Body { get; set; }
        public String Subject { get; set; }
    }
}
