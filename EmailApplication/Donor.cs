﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplication
{
    public class Donor : IUser
    {
        // Class members.
        //
        // Property.
        public String Name { get; set; }
        public String Email { get; set; }
        public String PhoneNo { get; set; }

        // Method.
        public IUser SaveUser() {
            // User created
            try {
                //Check if user already exist
                Logger.WriteLine("User Created");
                return this;
            }
            catch (Exception ex)
            {
                // return null if failed to save
                return null;
            }
        }

        public Donor SendEmail()
        {
            // User created
            try
            {
                //Check if user already exist
                return this;
            }
            catch (Exception ex)
            {
                // return null if failed to save
                return null;
            }
        }

        // Instance Constructor.
        public Donor(String name, String email, String phoneNo)
        {
            Name = name;
            Email = email;
            PhoneNo = phoneNo;
        }
    }
}
